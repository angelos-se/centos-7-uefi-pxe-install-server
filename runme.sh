#!/bin/bash

# Stop if something is wrong
set -e 

# Mount and test CentOS 7 ISO / optical disc
grep -q "mnt iso9660" /proc/mounts || mount /dev/sr0 /mnt
grep -q CentOS-7 /mnt/.treeinfo

# Install required packages
yum -y install dnsmasq grub2-efi-x64 shim vsftpd

# Save default dnsmasq.conf and install ours
mv /etc/dnsmasq.conf /etc/dnsmasq.conf-`date +%s`
cp dnsmasq.conf /etc/dnsmasq.conf

# Prepare tftpboot dir
mkdir -p /var/lib/tftpboot/centos7
cp /boot/efi/EFI/centos/grubx64.efi /boot/efi/EFI/centos/shim.efi /var/lib/tftpboot/
chmod +r /var/lib/tftpboot/*.efi
cp grub.cfg /var/lib/tftpboot/
cp /mnt/images/pxeboot/vmlinuz /mnt/images/pxeboot/initrd.img /var/lib/tftpboot/centos7

# Prepare ftp repo
mkdir /var/ftp/pub/centos7/
cp -r /mnt/* /var/ftp/pub/centos7/
chmod -R 755 /var/ftp/pub/centos7

# Reset SE Linux context for files
/sbin/restorecon -R -v /etc/dnsmasq.*
/sbin/restorecon -R -v /var/lib/tftpboot/

# Configure firewall and services
firewall-cmd --add-service=ftp --permanent  	## Port 21
firewall-cmd --add-service=dns --permanent  	## Port 53
firewall-cmd --add-service=dhcp --permanent  	## Port 67
firewall-cmd --add-port=69/udp --permanent  	## Port for TFTP
firewall-cmd --reload  ## Apply rules
systemctl enable --now dnsmasq vsftpd