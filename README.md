# CentOS 7 UEFI PXE Install Server

A minimally viable CentOS 7 configuration for netbooting CentOS 7 installer for UEFI clients

## References
- https://www.tecmint.com/install-pxe-network-boot-server-in-centos-7/
- https://ressman.org/posts/2018-05-06-pxe-boot-up-boards/

## Manual preparation from minimal install
- ~~Disable SELinux~~ (Fixed wrong context, we can now run with SE Liunx on)
- Configure DHCP listening interface (nmcli con add con-name eth1 ifname eth1 type ethernet ipv4.method manual ipv4.address 192.168.91.1/24)
- runme.sh (Or reproduce the steps manually :)
